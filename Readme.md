# Run

- [StartProject]
```bash
scrapy startproject cronjob
```

- [RunProject]
```bash
scrapy crawl cronjob
```

- [Json]
```bash
scrapy crawl cronjob -o items.json
```
